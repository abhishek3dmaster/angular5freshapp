

var request = require('request');
var http = require('http');
var apiurl = "https://dev.carbiqi.com/cbiqi/api/v1/";
var directory = "src/assets/data/";
request(apiurl+'faqs', function (error, response, faqbody) {
  var fs = require('fs');  
  var stream = fs.createWriteStream(directory+"faq.json");
  if(response && response.statusCode=="200"){
      stream.once('open', function(fd) {
            stream.write(faqbody);
            stream.end();
      });
  }  
})
request(apiurl+'testimonials', function (error, response, testimonialsbody) {
  var fs = require('fs');  
  var stream = fs.createWriteStream(directory+"testimonials.json");
  if(response && response.statusCode=="200"){
      stream.once('open', function(fd) {
            stream.write(testimonialsbody);
            stream.end();
      });
  }  
})
request(apiurl+'manufacturers', function (error, response, manufacturerbody) {
  var fs = require('fs');  
  var stream = fs.createWriteStream(directory+"manufacturer.json");
  if(response && response.statusCode=="200"){
      stream.once('open', function(fd) {
            stream.write(manufacturerbody);
            stream.end();
      });
  }  
})
request(apiurl+'kms', function (error, response, kmsbody) {
  var fs = require('fs');  
  var stream = fs.createWriteStream(directory+"kms.json");
  if(response && response.statusCode=="200"){
      stream.once('open', function(fd) {
            stream.write(kmsbody);
            stream.end();
      });
  }  
})
request('https://www.carbiqi.com/webcontent/ghost/api/v0.1/posts/?client_id=ghost-frontend&client_secret=7f781310a98c&include=tags&filter=tags:leading-locations&limit=5000', function (error, response, leadinglocations) {
  var fs = require('fs');  
  var stream = fs.createWriteStream(directory+"leading-locations.json");
  if(response && response.statusCode=="200"){
      stream.once('open', function(fd) {
            stream.write(leadinglocations);
            stream.end();
      });
  }  
})
request('https://www.carbiqi.com/webcontent/ghost/api/v0.1/posts/?client_id=ghost-frontend&client_secret=7f781310a98c&include=tags&filter=tags:leading-manufacturer&limit=5000', function (error, response, leadingmanufacturer) {
  var fs = require('fs');  
  var stream = fs.createWriteStream(directory+"leading-manufacturer.json");
  if(response && response.statusCode=="200"){
      stream.once('open', function(fd) {
            stream.write(leadingmanufacturer);
            stream.end();
      });
  }  
})
request('https://www.carbiqi.com/webcontent/ghost/api/v0.1/posts/?client_id=ghost-frontend&client_secret=7f781310a98c&include=tags&filter=tags:leading-models&limit=5000', function (error, response, leadingmodels) {
  var fs = require('fs');  
  var stream = fs.createWriteStream(directory+"leading-models.json");
  if(response && response.statusCode=="200"){
      stream.once('open', function(fd) {
            stream.write(leadingmodels);
            stream.end();
      });
  }  
})
request('https://www.carbiqi.com/webcontent/ghost/api/v0.1/posts/?client_id=ghost-frontend&client_secret=7f781310a98c&include=tags&filter=tags:industry-latest-news&limit=5000', function (error, response, leadingmodels) {
  var fs = require('fs');  
  var stream = fs.createWriteStream(directory+"industry-latest-news.json");
  if(response && response.statusCode=="200"){
      stream.once('open', function(fd) {
            stream.write(leadingmodels);
            stream.end();
      });
  }  
})
request('https://www.carbiqi.com/blogcontent/ghost/api/v0.1/posts/?client_id=ghost-frontend&client_secret=37e387dd0a41&include=author&include=tags&limit=5000', function (error, response, blogs) {
  var fs = require('fs');  
  var stream = fs.createWriteStream(directory+"blog.json");
  if(response && response.statusCode=="200"){
      stream.once('open', function(fd) {
            stream.write(blogs);
            stream.end();
      });
  }  
})

console.log("Records Updated Successfully")