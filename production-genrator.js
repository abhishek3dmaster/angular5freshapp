var ncp = require('ncp').ncp;

ncp.limit = 16;

ncp('dist', 'production/dist', function (err) {
 if (err) {
   return console.error(err);
 }
 console.log('dist folder copyied');
});
ncp('dist-server', 'production/dist-server', function (err) {
 if (err) {
   return console.error(err);
 }
 console.log('dist-server folder copyied');
});
ncp('server.js', 'production/server.js', function (err) {
 if (err) {
   return console.error(err);
 }
 console.log('server.js copyied');
});
ncp('package.json', 'production/package.json', function (err) {
 if (err) {
   return console.error(err);
 }
 console.log('package.json copyied');
});
